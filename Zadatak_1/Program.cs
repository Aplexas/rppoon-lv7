﻿using System;

namespace RPPOON_LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] numbers ={ 5, 4, 7, 2, 1, 0, 8, 9, 6, 3 };
            NumberSequence numberSequence1 = new NumberSequence(numbers);
            NumberSequence numberSequence2 = new NumberSequence(numbers);
            NumberSequence numberSequence3 = new NumberSequence(numbers);
            SortStrategy combSort = new CombSort();
            SortStrategy sequentialSort = new SequentialSort();
            SortStrategy bubbleSort = new BubbleSort();

            numberSequence1.SetSortStrategy(combSort);
            numberSequence2.SetSortStrategy(sequentialSort);
            numberSequence3.SetSortStrategy(bubbleSort);
            Console.WriteLine("Number sequence before sort: \n"+numberSequence1.ToString());
            numberSequence1.Sort();
            numberSequence2.Sort();
            numberSequence3.Sort();

            Console.WriteLine("Comb sort: \n"+numberSequence1.ToString());
            Console.WriteLine("Sequential sort: \n" + numberSequence2.ToString());
            Console.WriteLine("Bubble sort: \n" + numberSequence3.ToString());

        }
    }
}
