﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_2
{
    abstract interface SearchStrategy
    {
        public abstract void Search(double[] array, double searchedNumber);
       
    }
}
