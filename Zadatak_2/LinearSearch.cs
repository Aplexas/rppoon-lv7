﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_2
{
    class LinearSearch : SearchStrategy
    {
        public override void Search(double[] array, double searchedNumber)
        {
            int arraySize = array.Length;
            for (int i = 0; i < arraySize; i++)
            {
                if (array[i] == searchedNumber)
                {
                    Console.WriteLine("Number " + searchedNumber + " is found");
                    return;
                }
            }
            Console.WriteLine("Number " + searchedNumber + " is not found");

        }
    }
}
