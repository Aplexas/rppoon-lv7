﻿using System;

namespace Zadatak_2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] numbers = { 5, 4, 7, 2, 1, 0, 8, 9, 6, 3 };
            NumberSequence numberSequence = new NumberSequence(numbers);
            SearchStrategy linearSearch = new LinearSearch();
          
            numberSequence.SetSearchStrategy(linearSearch);
            Console.WriteLine("Number sequence: \n" + numberSequence.ToString());
            numberSequence.Search(4);
            numberSequence.Search(11);
        }
    }
}
