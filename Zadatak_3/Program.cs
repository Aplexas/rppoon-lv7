﻿using System;
using System.IO;

namespace Zadatak_3
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider systemDataProvider = new SystemDataProvider();
            Logger consoleLogger = new ConsoleLogger();
            Logger fileLogger = new FileLogger(@"C:\Users\Afaf\Documents\FileLogger.txt");
            systemDataProvider.Attach(consoleLogger);
            systemDataProvider.Attach(fileLogger);
            while (true)
            {
                systemDataProvider.GetAvailableRAM();
                systemDataProvider.GetCPULoad();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
