﻿using System;

namespace Zadatak_6
{
    class Program
    {
        static void Main(string[] args)
        {
      
            VHS vhs = new VHS("VHS tape", 20);
            DVD dvd = new DVD("Dvd tape",DVDType.SOFTWARE, 15);
            Book book = new Book("Book title", 10);
            IVisitor visitor = new RentVisitor();
            Console.WriteLine(vhs.ToString());
            Console.WriteLine(dvd.ToString());
            Console.WriteLine(book.ToString());

            Console.WriteLine(vhs.Accept(visitor));
            Console.WriteLine(dvd.Accept(visitor));
            Console.WriteLine(book.Accept(visitor));

        }
    }
}
